import os
import json
from occam import Occam

# Load OCCAM information
object = Occam.load()

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()
object_path = "/occam/%s-%s" % (object.id(), object.revision())


dependencies = object.dependencies()
qsim_source_code_path = ""
for dependency in dependencies:
    dep_type = dependency.get("type","")
    dep_subtype = dependency.get("subtype","")
    dep_name = dependency.get("name","")
    if(dep_name == "HMMSim") and (dep_type == "simulator"):
        hmmsim_source_code_path = "/occam/%s-%s" % (dependency.get("id",""), dependency.get("revision",""))


#########################################################
#                                                       #
#       The only input is the executable file           #
#                                                       #
#########################################################
inputs = object.inputs("trace")
if len(inputs) > 0:
    files = inputs[0].files()
    if len(files) > 0:
        input_file_path = inputs[0].files()[0]
    else:
        temp_input_path = os.path.join(inputs[0].volume(),"output.trace")
        if os.path.exists(temp_input_path):
            input_file_path=temp_input_path

#########################################################
#                                                       #
#  The output goes in this directory(see object.json)   #
#                                                       #
#########################################################
# Output file dir and path
output_path = os.path.join(object.path(), "new_output")
## Check if object is connected to another block
connection_to_trace = object.outputs("trace")
if len(connection_to_trace) > 0:
    output_block = connection_to_trace[0]
    output_path = output_block.volume()
## Create dir if needed
if not os.path.exists(output_path):
    os.mkdir(output_path);


#########################################################
#                                                       #
#                   Build run command                   #
#                                                       #
#########################################################
executable_path="Simulator/obj-intel64/convert"
executable=os.path.join(hmmsim_source_code_path,executable_path)
args=[
    "cd %s;"%(output_path),
    executable,
    input_file_path,
    "trace"
]
run_command = ' '.join(args)
Occam.report(run_command)
